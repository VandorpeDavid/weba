package be.winagent.weba.services;

import be.winagent.weba.domain.models.Bar;
import be.winagent.weba.domain.models.Item;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

public interface ItemService {
    Optional<Item> find(long id);
    Item create(Bar bar, Item item);
    Item update(Item item);
    List<Item> findAllByIds(Collection<Long> ids);
}
