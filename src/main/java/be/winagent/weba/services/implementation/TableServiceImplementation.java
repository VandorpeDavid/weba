package be.winagent.weba.services.implementation;

import be.winagent.weba.domain.models.Bar;
import be.winagent.weba.domain.models.Table;
import be.winagent.weba.domain.repositories.TableRepository;
import be.winagent.weba.services.TableService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.UUID;

@Service
@AllArgsConstructor
public class TableServiceImplementation implements TableService {
    private final TableRepository tableRepository;

    @Override
    public Optional<Table> find(UUID id) {
        return tableRepository.findById(id);
    }

    @Override
    public Table create(Bar bar, Table table) {
        table.setBar(bar);
        return tableRepository.save(table);
    }

    @Override
    public Table update(Table table) {
        return tableRepository.save(table);
    }
}
