package be.winagent.weba.services.implementation;


import be.winagent.weba.config.ApplicationConfig;
import be.winagent.weba.domain.models.Bar;
import be.winagent.weba.domain.models.Table;
import be.winagent.weba.domain.repositories.BarRepository;
import be.winagent.weba.services.BarService;
import lombok.AllArgsConstructor;
import net.glxn.qrgen.javase.QRCode;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.font.PDFont;
import org.apache.pdfbox.pdmodel.font.PDType1Font;
import org.apache.pdfbox.pdmodel.graphics.image.PDImageXObject;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Service;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Optional;
import java.util.UUID;

@Service
@AllArgsConstructor
public class BarServiceImplementation implements BarService {
    private final BarRepository barRepository;
    private final ApplicationConfig applicationConfig;

    @Override
    public Optional<Bar> find(UUID id) {
        return barRepository.findById(id);
    }

    @Override
    public Bar create(Bar bar) {
        return barRepository.save(bar);
    }

    @Override
    public Bar update(Bar bar) {
        return barRepository.save(bar);
    }

    @Override
    public ByteArrayInputStream generateTablePdf(Bar bar) throws IOException {
        int fontSize = 24;
        PDFont font = PDType1Font.HELVETICA_BOLD;
        int marginBottom = 30;
        int qrCodeSize = 500;

        PDDocument document = new PDDocument();
        for (Table table : bar.getTables()) {
            PDPage page = new PDPage();
            document.addPage(page);
            PDPageContentStream contentStream = new PDPageContentStream(document, page);

            String title = table.getName();
            float titleWidth = font.getStringWidth(title) / 1000 * fontSize;
            contentStream.setFont(font, fontSize);
            contentStream.beginText();
            contentStream.newLineAtOffset((page.getMediaBox().getWidth() - titleWidth) / 2, marginBottom);
            contentStream.showText(title);
            contentStream.endText();

            float pageWitdh = page.getMediaBox().getUpperRightX() - page.getMediaBox().getLowerLeftX();
            float pageHeight = page.getMediaBox().getUpperRightY() - page.getMediaBox().getLowerLeftY();
            ByteArrayOutputStream img = QRCode
                    .from(
                            String.format("%s/orders/create?table=%s", applicationConfig.getBaseUrl(), table.getId())
                    )
                    .withSize(qrCodeSize, qrCodeSize)
                    .stream();
            PDImageXObject image = PDImageXObject.createFromByteArray(
                    document,
                    img.toByteArray(),
                    String.format("QRcode table %s", table.getName())
            );
            contentStream.drawImage(image, (pageWitdh - qrCodeSize)/2, (pageHeight - qrCodeSize)/2);
            contentStream.close();
        }

        ByteArrayOutputStream out = new ByteArrayOutputStream();
        document.save(out);
        document.close();
        return new ByteArrayInputStream(out.toByteArray());
    }
}
