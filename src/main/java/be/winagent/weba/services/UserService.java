package be.winagent.weba.services;

import be.winagent.weba.domain.models.User;

import java.util.Optional;

public interface UserService {
    User updateOrCreateUser(User user);
    Optional<User> find(Long id);
    Optional<User> findByUsername(String username);

}
