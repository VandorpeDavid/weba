package be.winagent.weba.services;

import be.winagent.weba.domain.models.Bar;
import be.winagent.weba.domain.models.Table;

import java.util.Optional;
import java.util.UUID;

public interface TableService {
    Optional<Table> find(UUID id);
    Table create(Bar bar, Table table);
    Table update(Table table);
}
