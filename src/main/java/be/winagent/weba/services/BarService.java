package be.winagent.weba.services;

import be.winagent.weba.domain.models.*;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.Optional;
import java.util.UUID;

public interface BarService {
    Optional<Bar> find(UUID id);
    Bar create(Bar bar);
    Bar update(Bar bar);

    ByteArrayInputStream generateTablePdf(Bar bar) throws IOException;
}
