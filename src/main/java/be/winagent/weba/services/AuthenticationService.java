package be.winagent.weba.services;

import be.winagent.weba.domain.models.User;
import org.springframework.security.core.Authentication;
import java.util.Optional;

public interface AuthenticationService {
    Optional<User> getUser(Authentication authentication);
}
