package be.winagent.weba.controllers.forms.converter;

import be.winagent.weba.controllers.forms.models.BarForm;
import be.winagent.weba.domain.models.Bar;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@AllArgsConstructor
public class BarConverter implements BidirectionalConverter<Bar, BarForm> {
    @Override
    public Bar update(Bar bar, BarForm barForm) {
        bar.setName(barForm.getName());
        bar.setOpen(barForm.isOpen());
        return bar;
    }

    @Override
    public Bar build(BarForm barForm) {
        return update(new Bar(), barForm);
    }

    @Override
    public BarForm reverseUpdate(BarForm target, Bar source) {
        target.setName(source.getName());
        target.setOpen(source.isOpen());
        return target;
    }

    @Override
    public BarForm reverseBuild(Bar source) {
        return reverseUpdate(new BarForm(), source);
    }
}
