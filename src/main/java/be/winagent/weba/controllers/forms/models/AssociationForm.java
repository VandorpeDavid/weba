package be.winagent.weba.controllers.forms.models;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotEmpty;

@Getter
@Setter
public class AssociationForm {

    @NotEmpty
    @Length(max = 255)
    private String abbreviation;

    @NotEmpty
    @Length(max = 255)
    private String name;
}
