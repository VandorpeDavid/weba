package be.winagent.weba.controllers.forms.models;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Getter
@Setter
public class BarForm {
    @NotEmpty
    @Length(max = 255)
    private String name;

    @NotNull
    private boolean open;
}
