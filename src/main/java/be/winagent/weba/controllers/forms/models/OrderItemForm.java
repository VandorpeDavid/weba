package be.winagent.weba.controllers.forms.models;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

@Getter
@Setter
public class OrderItemForm {
    @NotNull
    private Long itemId;

    @Min(0)
    @NotNull
    private Integer amount = 0;
}
