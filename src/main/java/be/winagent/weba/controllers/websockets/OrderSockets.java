package be.winagent.weba.controllers.websockets;

import be.winagent.weba.domain.models.Order;

public interface OrderSockets {
    public void publish(Order order);
    public void publish(Order order, boolean updateQueue);
}
