package be.winagent.weba.controllers;

import be.winagent.weba.controllers.annotations.Required;
import be.winagent.weba.controllers.exceptions.NotFoundException;
import be.winagent.weba.controllers.forms.converter.Converter;
import be.winagent.weba.controllers.forms.models.ItemForm;
import be.winagent.weba.controllers.forms.models.OrderForm;
import be.winagent.weba.controllers.forms.models.OrderItemForm;
import be.winagent.weba.domain.models.*;
import be.winagent.weba.services.OrderService;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Valid;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/orders")
@AllArgsConstructor
public class OrderController extends ApplicationController {
    private final OrderService orderService;
    private final Converter<Order, OrderForm> formConverter;

    @GetMapping
    @PreAuthorize("isBarAdmin(#bar)")
    public String index(Model model, @Required Bar bar, Pageable pageable) {
        model.addAttribute("orders", orderService.listOrders(bar, pageable));
        return "orders/index";
    }

    @GetMapping("/show")
    public String show(Model model, @Required Order order) {
        model.addAttribute("queuePosition", orderService.getQueuePosition(order));
        return "orders/show";
    }

    @GetMapping("/todo")
    @PreAuthorize("isBarAdmin(#bar)")
    public String todo(@Required Bar bar) {
        return "orders/todo";
    }

    @PostMapping("/start")
    @PreAuthorize("isOrderAdmin(#order)")
    public ResponseEntity<String> start(@Required Order order) {
        orderService.start(order);
        return ResponseEntity.ok("ok");
    }

    @PostMapping("/reset")
    @PreAuthorize("isOrderAdmin(#order)")
    public ResponseEntity<String> reset(@Required Order order) {
        orderService.reset(order);
        return ResponseEntity.ok("ok");
    }

    @PostMapping("/complete")
    @PreAuthorize("isOrderAdmin(#order)")
    public ResponseEntity<String> complete(@Required Order order) {
        orderService.complete(order);
        return ResponseEntity.ok("ok");
    }

    @PostMapping("/cancel")
    @PreAuthorize("isOrderAdmin(#order)")
    public ResponseEntity<String> cancel(@Required Order order) {
        orderService.cancel(order);
        return ResponseEntity.ok("ok");
    }

    @GetMapping("/create")
    public String createForm(Model model, @Required Table table, OrderForm orderForm) {
        if (!table.getBar().isOpen()) {
            model.addAttribute("bar", table.getBar());
            return "bars/closed";
        }

        List<Item> items = getItems(table.getBar());
        model.addAttribute("items", items);
        orderForm.setItems(
                items.stream()
                        .map((item) -> {
                            OrderItemForm orderItem = new OrderItemForm();
                            orderItem.setAmount(0);
                            orderItem.setItemId(item.getId());
                            return orderItem;
                        })
                        .collect(Collectors.toList())
        );
        return "orders/create";
    }

    private List<Item> getItems(Bar bar) {
        return bar.getItems().stream()
                .filter(Item::isAvailable)
                .sorted(Comparator.comparing(Item::getName))
                .collect(Collectors.toList());
    }

    @PostMapping("/create")
    public String create(Model model, @Valid OrderForm orderForm, BindingResult bindingResult, @Required Table table) {
        if (!table.getBar().isOpen()) {
            model.addAttribute("bar", table.getBar());
            return "bars/closed";
        }

        if (bindingResult.hasErrors()) {
            List<Item> items = getItems(table.getBar());
            model.addAttribute("items", items);
            Map<Long, Integer> current = orderForm.getItems().stream()
                    .collect(Collectors.toMap(
                            OrderItemForm::getItemId,
                            OrderItemForm::getAmount
                    ));
            orderForm.getItems().addAll(
                    items.stream()
                            .map((item) -> {
                                OrderItemForm orderItem = new OrderItemForm();
                                orderItem.setAmount(current.getOrDefault(item.getId(), 0));
                                orderItem.setItemId(item.getId());
                                return orderItem;
                            })
                            .collect(Collectors.toList())
            );
            return "orders/create";
        }

        Order order = formConverter.build(orderForm);
        order.getItems().removeIf((item) -> !item.getItem().getBar().getId().equals(table.getBar().getId()));
        order.getItems().removeIf((item) -> !item.getItem().isAvailable());

        order = orderService.create(table, order);
        return redirect(order);
    }

    public static String redirect(Order order) {
        return redirect(order, "show");
    }

    public static String redirect(Order order, String action) {
        return String.format("redirect:/orders/%s?order=%s", action, order.getId());
    }
}
