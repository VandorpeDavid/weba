package be.winagent.weba.controllers;

import be.winagent.weba.controllers.annotations.Required;
import be.winagent.weba.controllers.forms.converter.BidirectionalConverter;
import be.winagent.weba.controllers.forms.models.BarForm;
import be.winagent.weba.domain.models.*;
import be.winagent.weba.services.BarService;
import lombok.AllArgsConstructor;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.font.PDType1Font;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Comparator;

@Controller
@RequestMapping("/bars")
@AllArgsConstructor
public class BarController extends ApplicationController {
    private final BarService barService;
    private final BidirectionalConverter<Bar, BarForm> formConverter;

    @GetMapping("/show")
    @PreAuthorize("isBarAdmin(#bar)")
    public String show(@Required Bar bar) {
        bar.getTables().sort(Comparator.comparing(Table::getName));
        bar.getItems().sort(Comparator.comparing(Item::getName));
        return "bars/show";
    }

    @GetMapping(value = "/qrcodes", produces = MediaType.APPLICATION_PDF_VALUE)
    @PreAuthorize("isBarAdmin(#bar)")
    public ResponseEntity<InputStreamResource> qrcodes(@Required Bar bar) throws IOException {
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Disposition", String.format("inline; filename=qrcodes-%s.pdf", bar.getId()));
        ByteArrayInputStream in = barService.generateTablePdf(bar);
        return ResponseEntity
                .ok()
                .headers(headers)
                .contentType(MediaType.APPLICATION_PDF)
                .body(new InputStreamResource(in));
    }

    @GetMapping("/create")
    @PreAuthorize("isAssociationAdmin(#association)")
    public String createForm(@Required Association association, BarForm barForm) {
        return "bars/create";
    }

    @PostMapping("/create")
    @PreAuthorize("isAssociationAdmin(#association)")
    public String create(@Valid BarForm barForm, BindingResult bindingResult, @Required Association association) {
        if (bindingResult.hasErrors()) {
            return "bars/create";
        }

        Bar bar = formConverter.build(barForm);
        bar.setAssociation(association);
        bar = barService.create(bar);
        return redirect(bar);
    }

    @GetMapping("/edit")
    @PreAuthorize("isBarAdmin(#bar)")
    public String editForm(Model model, Bar bar) {
        model.addAttribute("barForm", formConverter.reverseBuild(bar));
        return "bars/edit";
    }

    @PostMapping("/edit")
    @PreAuthorize("isBarAdmin(#bar)")
    public String edit(@Valid BarForm barForm, BindingResult bindingResult, @Required Bar bar) {
        if (bindingResult.hasErrors()) {
            return "bars/edit";
        }

        bar = barService.update(formConverter.update(bar, barForm));
        return redirect(bar);
    }

    public static String redirect(Bar bar) {
        return redirect(bar, "show");
    }

    public static String redirect(Bar bar, String action) {
        return String.format("redirect:/bars/%s?bar=%s", action, bar.getId());
    }
}
