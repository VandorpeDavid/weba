package be.winagent.weba.controllers;

import be.winagent.weba.controllers.exceptions.NotFoundException;
import be.winagent.weba.domain.models.*;
import be.winagent.weba.services.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Optional;
import java.util.UUID;

public abstract class ApplicationController {
    @Autowired
    private AssociationService associationService;

    @Autowired
    private BarService barService;

    @Autowired
    private ItemService itemService;

    @Autowired
    private OrderService orderService;

    @Autowired
    private TableService tableService;

    @Autowired
    private AuthenticationService authenticationService;

    @ModelAttribute(binding = false)
    public Association addAssociation(@RequestParam(required = false, name = "association") String abbreviation) {
        return Optional.ofNullable(abbreviation)
                .map(
                        (abbrev) -> associationService
                                .findByAbbreviation(abbrev)
                                .orElseThrow(NotFoundException::new)
                )
                .orElse(null);
    }

    @ModelAttribute(binding = false)
    public Bar addBar(@RequestParam(required = false, name = "bar") UUID barId) {
        return Optional.ofNullable(barId)
                .map(
                        (id) -> barService
                                .find(id)
                                .orElseThrow(NotFoundException::new)
                )
                .orElse(null);
    }

    @ModelAttribute(binding = false)
    public Item addItem(@RequestParam(required = false, name = "item") Long itemId) {
        return Optional.ofNullable(itemId)
                .map(
                        (id) -> itemService
                                .find(id)
                                .orElseThrow(NotFoundException::new)
                )
                .orElse(null);
    }

    @ModelAttribute(binding = false)
    public Table addTable(@RequestParam(required = false, name = "table") UUID tableId) {
        return Optional.ofNullable(tableId)
                .map(
                        (id) -> tableService
                                .find(id)
                                .orElseThrow(NotFoundException::new)
                )
                .orElse(null);
    }

    @ModelAttribute(binding = false)
    public Order addOrder(@RequestParam(required = false, name = "order") UUID orderId) {
        return Optional.ofNullable(orderId)
                .map(
                        (id) -> orderService
                                .find(id)
                                .orElseThrow(NotFoundException::new)
                )
                .orElse(null);
    }

    @ModelAttribute(binding = false, name = "currentUser")
    public User addCurrentUser(Authentication authentication) {
        return authenticationService.getUser(authentication).orElse(null);
    }
}
