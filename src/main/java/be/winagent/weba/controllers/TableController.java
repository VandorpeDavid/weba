package be.winagent.weba.controllers;

import be.winagent.weba.controllers.annotations.Required;
import be.winagent.weba.controllers.forms.converter.BidirectionalConverter;
import be.winagent.weba.controllers.forms.models.TableForm;
import be.winagent.weba.domain.models.Bar;
import be.winagent.weba.domain.models.Table;
import be.winagent.weba.services.TableService;
import lombok.AllArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Valid;

@Controller
@RequestMapping("/tables")
@AllArgsConstructor
public class TableController extends ApplicationController {
    private final TableService tableService;
    private final BidirectionalConverter<Table, TableForm> formConverter;

    @GetMapping("/create")
    @PreAuthorize("isBarAdmin(#bar)")
    public String createForm(@Required Bar bar, TableForm tableForm) {
        return "tables/create";
    }

    @PostMapping("/create")
    @PreAuthorize("isBarAdmin(#bar)")
    public String create(@Valid TableForm tableForm, BindingResult bindingResult, @Required Bar bar) {
        if (bindingResult.hasErrors()) {
            return "tables/create";
        }

        Table table = formConverter.build(tableForm);
        table = tableService.create(bar, table);
        return redirect(table);
    }

    @GetMapping("/edit")
    @PreAuthorize("isTableAdmin(#table)")
    public String editForm(Model model, Table table) {
        model.addAttribute("tableForm", formConverter.reverseBuild(table));
        return "tables/edit";
    }

    @PostMapping("/edit")
    @PreAuthorize("isTableAdmin(#table)")
    public String edit(@Valid TableForm tableForm, BindingResult bindingResult, @Required Table table) {
        if (bindingResult.hasErrors()) {
            return "tables/edit";
        }

        table = tableService.update(formConverter.update(table, tableForm));
        return redirect(table);
    }

    public String redirect(Table table) {
        return BarController.redirect(table.getBar());
    }
}
