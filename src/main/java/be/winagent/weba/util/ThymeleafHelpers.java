package be.winagent.weba.util;

import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.function.Function;

@Component
public class ThymeleafHelpers {
    @Bean
    public Function<Integer, String> money() {
        return (amount) -> String.format("€ %.2f", amount / 100.0);
    }

    @Bean
    public Function<LocalDateTime, String> datetime() {
        return DateTimeFormatter.ofPattern("HH:mm dd/MM/yyyy")::format;
    }
}
