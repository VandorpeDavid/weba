package be.winagent.weba.domain.repositories;

import be.winagent.weba.domain.models.Order;
import be.winagent.weba.domain.models.OrderStatus;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;

import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

public interface OrderRepository extends CrudRepository<Order, UUID> {
    Page<Order> findAllByTableBarIdOrderByCreated(UUID barId, Pageable pageable);

    List<Order> findAllByTableBarIdAndStatusIsIn(UUID barId, Iterable<OrderStatus> statuses);

    int countAllByTableBarIdAndStatusIsInAndCreatedIsBefore(UUID barId, Iterable<OrderStatus> statuses, LocalDateTime created);
}
