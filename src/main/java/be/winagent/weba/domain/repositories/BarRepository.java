package be.winagent.weba.domain.repositories;

import be.winagent.weba.domain.models.Bar;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface BarRepository extends CrudRepository<Bar, UUID> {
}
