package be.winagent.weba.domain.repositories;

import be.winagent.weba.domain.models.Table;
import org.springframework.data.repository.CrudRepository;

import java.util.UUID;

public interface TableRepository extends CrudRepository<Table, UUID> {
}
