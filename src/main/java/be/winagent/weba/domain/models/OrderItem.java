package be.winagent.weba.domain.models;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.util.UUID;

@Getter
@Setter
@Entity
public class OrderItem {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @ManyToOne
    @NotNull
    private Order order;

    @ManyToOne(fetch = FetchType.EAGER)
    @NotNull
    private Item item;

    @Positive
    private int amount;

    public int getSubTotal() {
        return amount * item.getPrice();
    }
}
