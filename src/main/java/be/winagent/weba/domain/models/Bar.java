package be.winagent.weba.domain.models;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Entity
@Getter
@Setter
public class Bar {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private UUID id;

    @NotNull
    @ManyToOne
    private Association association;

    @NotEmpty
    @Length(max = 255)
    private String name;

    @OneToMany(mappedBy = "bar")
    private List<Table> tables = new ArrayList<>();

    @OneToMany(mappedBy = "bar")
    private List<Item> items = new ArrayList<>();

    @CreationTimestamp
    private LocalDateTime created;

    private boolean open;
}
